import { Routes } from '@angular/router';

import {
    HomePageComponent, BandPageComponent, AlbumPageComponent, CommentPageComponent
} from './pages';


export const appRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomePageComponent
    },
    {
        path: 'band/:bandId',
        component: BandPageComponent,
        children: [{
            path: 'album/:albumId',
            component: AlbumPageComponent,
            children: [{
                path: 'track/:trackId',
                component: CommentPageComponent,
            }],
        }],
    },
    { path: '**', redirectTo: '/' },
];
