import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ApiService } from '../../services';

@Component({
    selector: 'album-page',
    templateUrl: './album-page.component.html',
    styleUrls: ['./album-page.component.scss']

})
export class AlbumPageComponent implements OnInit, OnDestroy {
    album: any;
    tracks: Promise<any>;
    paramsSub: any;

    constructor(
        private service: ApiService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => {
            this.album = params['albumId'];
            this.tracks = this.service.getTracks(this.album);
        });
    }

    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }
}
