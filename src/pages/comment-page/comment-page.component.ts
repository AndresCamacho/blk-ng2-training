import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ApiService } from '../../services';



@Component({
    selector: 'comment-page',
    templateUrl: './comment-page.component.html',
    styleUrls: ['./comment-page.component.scss']

})
export class CommentPageComponent implements OnInit, OnDestroy {
    trackId: string;
    comments: Promise<any>;
    paramsSub: any;

    constructor(
        private service: ApiService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => {
            this.trackId = params['trackId'];
            if (this.trackId) {
                this.comments = this.service.getCommentsForTrack(this.trackId);
            }
        });
    }

    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }
}
