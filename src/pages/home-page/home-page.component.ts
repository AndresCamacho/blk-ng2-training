import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api';



@Component({
    selector: 'home-page',
    template: `
        <table-list type="band" filter
            [paginate]="5"
            [columns]="[
                {key: 'name'},
                {key: 'genres'},
                {key: 'popularity'}
            ]"
            [data]="data | async">
        </table-list>
        <!-- <pre>{{data | json}}</pre> -->
    `,
})
export class HomePageComponent implements OnInit {
    data: any;

    constructor(private apiService: ApiService) {
    }

    ngOnInit() {
        this.data = this.apiService.getBands();
    }
}
