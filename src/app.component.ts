import { Component } from '@angular/core';


@Component({
    selector: 'app',
    template: `
        <div ng-view class="panel-body row">
            <router-outlet></router-outlet>
        </div>
    `,
    styles: [`
        @import '/node_modules/bootstrap/dist/css/bootstrap.css';
    `]
})
export class AppComponent {}
