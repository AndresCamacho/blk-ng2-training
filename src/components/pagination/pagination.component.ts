import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})

export class PaginationComponent implements OnChanges {

    @Input() size: number;
    @Input() data: Array<object>;
    @Input() filter?: string = '';
    @Input() filterAttribute?: string = 'name';

    currentPage: number = 1;
    lastPage: number = null;
    pageSize: number = 10;
    prevEnabled: boolean = true;
    nextEnabled: boolean = true;
    filteredData: Array<object>;

    slicedData: any;
    @Output() slicedChange: EventEmitter<any>;


    @Input()
    get sliced() {
        return this.slicedData;
    };

    set sliced(val) {
        if (val) {
            this.slicedData = val;
            this.slicedChange.emit(this.slicedData);
        }
    };

    constructor() {
        this.slicedChange = new EventEmitter<any>(true);
    };

    ngOnChanges(changes) {
        if ((changes['data'] || changes['filter']) && this.data) {
            this.size = this.size || this.pageSize;
            this.filteredData = this.filter ? this.setFilter(this.data, this.filterAttribute, this.filter) : this.data;
            this.lastPage = Math.ceil(this.filteredData.length / this.size);
            this.sliced = this.setPage(this.filteredData, this.size, this.currentPage);
        }
    };

    prevPage() {
        if (this.currentPage > 1) {
            this.currentPage--;
            this.sliced = this.setPage(
                this.filter ? this.setFilter(this.data, this.filterAttribute, this.filter) : this.data,
                this.size,
                this.currentPage);
        }
    };

    nextPage() {
        if (this.currentPage < this.lastPage) {
            this.currentPage++;
            this.sliced = this.setPage(
                this.filter ? this.setFilter(this.data, this.filterAttribute, this.filter) : this.data,
                this.size,
                this.currentPage);
        }
    };

    setFilter(data, filterAttribute, filter) {
        return data
            .filter(item => (item[filterAttribute].toUpperCase().indexOf(filter.toUpperCase() || '') != -1));
    }

    setPage(data, size, currentPage) {
        return data
            .slice((currentPage - 1) * size, ((currentPage - 1) * size) + size);
    };
}

