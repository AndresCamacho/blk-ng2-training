import { Component, Input, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'vertical-list',
    templateUrl: './vertical-list.component.html',
    styleUrls: ['./vertical-list.component.scss'],
})
export class VerticalListComponent {

    @Input()
    data: Array<any>;

    @Input()
    size: number = 3;

    @Input()
    paginate?: boolean = false;

    @Input()
    type: string;

    @Input()
    filterAttribute?: string = 'name';

    slicedData: any;
}
