import { Component, Input } from '@angular/core';

import { TableColumns } from './table-columns.interface';



@Component({
  selector: 'table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss']
})
export class TableListComponent {
  @Input()
  columns: Array<TableColumns> = [];

  @Input()
  data: Array<any>;

  @Input()
  paginate?: boolean = false;

  @Input()
  size: number = 3;

  @Input()
  type: string;

  filter: string = '';
  slicedData: any;

  onEnter(value) {
    this.filter = value;
  }
}
