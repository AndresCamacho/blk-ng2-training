import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ApiService } from '../../services';



@Component({
    selector: 'comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss']

})
export class CommentsComponent {
    @Input() data: Promise<any>;
    @Input() track: string;
    comments: Array<any>;

    commentField: string;
    trackId: string;

    constructor(
        private service: ApiService,
        private route: ActivatedRoute,
    ) {
    }

    onSubmit() {
        this.service.postCommentForTrack(this.track, {
            id: this.createGuid(),
            message: this.commentField,
            name: 'placeholder',
            ts: new Date(),
            trackId: this.route.snapshot.params['trackId'],
        })
            .then(response => {
                //   this.route.reload();
                window.location.reload();
                return response.data;
            });
    }

    createGuid() {
        return 'xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

}
